import { Compiler } from "./compiler.js";
import { Checker } from "./checker.js";

const checker = new Checker;
const compiler = new Compiler;
/**
 * Add action on load of the browser page
 */
window.onload = function () {
    // Initialize compile button to allow document compiling
    let compileButton = document.getElementById('compile');
    compileButton.onclick = compiler.compile;
    // Find all inputs on the DOM which are bound to a datalist via their list attribute.
    var inputsList = document.querySelectorAll('input[list]');
    checker.checkInputListsValidity(inputsList);
}

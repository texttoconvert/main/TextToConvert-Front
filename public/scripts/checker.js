

/**
 * @class Checker
 */
export function Checker() {

    /**
     * Add a check of user input on input lists on events
     * @param {Array} inputsList an array of input lists of type datalist to check
     */
    this.checkInputListsValidity = function (inputsList) {
        for (var i = 0; i < inputsList.length; i++) {
            // When the value of the input changes...
            inputsList[i].addEventListener('change', this.checkListValidity);
            inputsList[i].addEventListener('change', this.checkListValidity);
        }
    }

    /**
     * Checks that user input exists on datalist for the input area
     * @param {Array} inputsList an array of input of type datalist to check
     */
    this.checkListValidity=function () {
        var optionFound = false,
            datalist = this.list;
        // Determine whether an option exists with the current value of the input.
        for (var j = 0; j < datalist.options.length; j++) {
            if (this.value == datalist.options[j].value) {
                optionFound = true;
                break;
            }
        }
        // use the setCustomValidity function of the Validation API
        // to provide an user feedback if the value does not exist in the datalist
        if (optionFound) {
            this.setCustomValidity('');
        } else {
            this.setCustomValidity('Please select a valid value.');
        }
    }
};
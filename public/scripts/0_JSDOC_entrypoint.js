/**
 * # TextToConvert-Front JavaScript code documentation
 * 
 * This documentation is only a technical code oriented documentation.
 * 
 * It has been generated using [DocumentationJS](https://github.com/documentationjs/documentation).
 *  
 * ## Usefull links
 * 
 * - [TextToConvert main documentation](https://texttoconvert.gitlab.io/TextToConvert-Documentation/)
 * - [TextToConvert-Front documentation](https://texttoconvert.gitlab.io/TextToConvert-Documentation/TextToConvert-Front/)
 * - [TextToConvert-Front repository](https://gitlab.com/texttoconvert/main/TextToConvert-Front)
 * - [TextToConvert-Front demo website](https://texttoconvert.gitlab.io/main/TextToConvert-Front/)
 * 
 * *Introduction function does nothing. It has only been created to add a table of content entry to this documentation.*
 */
function Introduction(){}
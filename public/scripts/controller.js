/**
 * @class Controller
 */
export function Controller() {

    /**
     * Send content with its format and expected output format to retrieve the content converted to the expected format
     * @param {Object} dataToConvert JSON object with content type, expected output type and content:
     * @param {Object} dataToConvert.parameters Document compilation parameters
     * @param {string} dataToConvert.parameters.inputFormat Document input format to compile from
     * @param {string} dataToConvert.parameters.outputFormat Document output format to compile to
     * @param {Object} dataToConvert.data Document data
     * @param {string} dataToConvert.data.input Document text input
     * @return {Promise<Blob>} a promise of the expected converted content
     */
    this.composeDocument = function (dataToConvert) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        return fetch(`https://texttoconvert.herokuapp.com/compile`, {
            method: "POST",
            mode: "cors",
            cache: 'default',
            headers: myHeaders,
            body: JSON.stringify(dataToConvert)
        })
    };
}
import { Controller } from "./controller.js";

const controller = new Controller;

/**
 * @class Compiler
 */
export function Compiler() {
    const compiler = this;

    /**
     * Compile text area input to selected document format.
     * Display the compiled document on the preview pannel if possible.
     * If document display is not possible let the browser take action.
     */
    this.compile = function () {
        let request = {
            parameters: {
                inputFormat: document.getElementById("inputFormat").value || "md",
                outputFormat: document.getElementById("outputFormat").value || "html"
            },
            data: {
                input: document.getElementById("textToConvert").value || '### Your document is empty for now!\n**Default input format is "markdown" and output format "HTML"**'
            }
        };
        controller.composeDocument(request).then((response) => {
            response.blob().then((file) => {
                let fileURL = URL.createObjectURL(file);
                compiler.showFileInPreview(fileURL);
                compiler.allowDownload(fileURL);
                compiler.allowOpenInNewTab(fileURL);
            }).catch((error) => {
                alert("An error occured:\n" + error.message);
                console.error(error);
            });
        }).catch((error) => {
            alert("An error occured:\n" + error.message);
            console.error(error);
        });;
    };

    /**
     * Show the compiled file on the preview panel
     * @param {String} fileURL The URL of the file to display
     */
    this.showFileInPreview = function (fileURL) {
        var iframeViewer = document.getElementById("iframeViewer");
        iframeViewer.src = fileURL;
    };

    /**
     * Allow download of the compiled file shown on the preview panel
     * @param {String} fileURL The URL of the file to download
     */
    this.allowDownload = function (fileURL) {
        var downloadFile = document.getElementById("downloadFile");
        downloadFile.href = fileURL;
        downloadFile.download = 'document';
        downloadFile.style.backgroundColor = "rgb(61,153,113)";
        downloadFile.style.color = "white";
        downloadFile.classList.remove("buttonDisabled");
        downloadFile.classList.add("buttonEnabled");
    };

    /**
     * Allow openning compiled file in new tab
     * @param {String} fileURL The URL of the file to open
     */
    this.allowOpenInNewTab = function (fileURL) {
        var openFile = document.getElementById("openFile");
        openFile.href = fileURL;
        openFile.target = '_blank';
        openFile.style.backgroundColor = "rgb(61,153,113)";
        openFile.style.color = "white";
        openFile.classList.remove("buttonDisabled");
        openFile.classList.add("buttonEnabled");
    }
}